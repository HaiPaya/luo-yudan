console.log("正在启动罗寓丹离线版...")
const fs = require("fs")
const { Logger } = require("./modules/.logger")
const logger = new Logger("Main")
const { Events } = require("./modules/.Events")

//获取插件列表
logger.info("正在加载模块...")
var modulesList = []
fs.readdirSync("./modules").filter(file => file.indexOf(".js") !== -1).forEach(file => {
    modulesList.push(file)
})

//加载插件
var modulesObjList = []
for(let i = 0; modulesList[i]; i++) {
    modulesObjList[i] = require(`./modules/${modulesList[i]}`)
    modulesObjList[i].file = modulesList[i]
    logger.info(`模块 ${modulesObjList[i].name}[${modulesList[i]}]-${modulesObjList[i].version} 加载完毕!`)
}

logger.info("加载完毕!")
Events.on("sys.start")