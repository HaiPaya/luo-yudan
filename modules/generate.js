/*QQ机器人前置*/
const { Logger } = require("./.logger")
const logger = new Logger("DChatGLM")
const request = require('request')
const { Events } = require("./.Events")
const Config = require('../Config.json')
const fs = require('fs')

//获取时间
function getFormattedDate() {
    let now = new Date()
    let year = now.getFullYear()
    let month = (now.getMonth() + 1).toString().padStart(2, '0')
    let day = now.getDate().toString().padStart(2, '0')
    let hours = now.getHours().toString().padStart(2, '0')
    let minutes = now.getMinutes().toString().padStart(2, '0')
    let seconds = now.getSeconds().toString().padStart(2, '0')
    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
}

let httpPath = "https://dashscope.aliyuncs.com/api/v1/services/aigc/text-generation/generation"
let model = "qwen-max"
let parameters = {
    "top_p": 0.1, //0-1.0 随机性
    "top_k": 10,//0-100 随机性
    "temperature": 1.5,//0-2 多样性
    "repetition_penalty": 1.0,//1.0-～ 重复惩罚
    "enable_search": true//搜索
}

function getSysRole() { return`
你在扮演名为罗寓丹的助理AI，你要学习人类说话的语气，回复不要超过30字，不要使用md格式。

你需要服侍我，我的基础信息：${Config["用户信息"]}

你可以回复json来执行操作，例如：[{"操作":"操作名称","参数":{参数}}]，可以同时执行多个操作，请在执行前确认我有这样的需求并且提供给你了详细信息，没有的话请询问后在单独执行，目前可用的操作有：
- 添加倒计时 {"操作":"添加倒计时","参数":{"time": "时间(秒)", "name": "倒计时名称“}}

你需要根据我的需求执行操作，在你JSON后系统会执行操作并拦截你的输出给我，在操作执行完后会将执行结果返回给你，你需要根据结果汇报
请记住：如果你没有确认我的需求和操作的参数一定不要回复json，并询问我。
当前时间：${getFormattedDate()}
`}

let chatList = []
//对话
function chat(msg, fun, isR = false) {
    try {
        //构建内容
        let ncontent = [{"text": `${msg}`}]

        //判断是否已经创建对话
        if(chatList.length === 0) {
            //没有
            chatList = [
                {
                    "role": "system",
                    "content": [
                        {
                            "text": getSysRole()
                        }
                    ]
                },
                {
                    "role": "user",
                    "content": ncontent
                }
            ]
            request.post(httpPath, {
                "headers": {
                    "Content-Type": "application/json",
                    "Authorization": Config["推理"].apiKey
                },
                "json": {
                    "model": model,
                    "input": {
                        "messages": chatList
                    },
                    "parameters": parameters
                }
            }, (err, res, body) => {
                //logger.info(JSON.stringify(body))
                if (err) {
                    logger.error(`请求出错:${err}`)
                    fun()
                    return
                }
                if(body.output == null) {
                    logger.error(`请求出错:${JSON.stringify(body)}`)
                    fun()
                    return
                }
                chatList.push({
                    "role": "assistant",
                    "content": body.output.text
                })
                echat(body.output.text, fun, isR)
            })
        }
        else {
            //有
            //处理历史记录
            if(chatList.length >= (Config["推理"]["记忆聊天(条)"] * 2) + 1) {
                chatList.splice(1, 2)
            }

            chatList[0].content[0].text = getSysRole()
            chatList.push({
                "role": "user",
                "content": ncontent
            })

            request.post(httpPath, {
                "headers": {
                    "Content-Type": "application/json",
                    "Authorization": Config["推理"].apiKey
                },
                "json": {
                    "model": model,
                    "input": {
                        "messages": chatList
                    },
                    "parameters": parameters
                }
            }, (err, res, body) => {
                //logger.info(JSON.stringify(body))
                if (err) {
                    logger.error(`请求出错:${err}`)
                    fun()
                    return
                }
                if(body.output == null) {
                    logger.error(`请求出错:${JSON.stringify(body)}`)
                    fun()
                    return
                }
                chatList.push({
                    "role": "assistant",
                    "content": body.output.text
                })
                echat(body.output.text, fun, isR)
            })
        }
    }
    catch(err) {
        logger.error(`请求出错:${err}`)
        fun()
    }
}

//后处理
function echat(msg, rFun, isR) {
    if(isR) {
        rFun(msg)
        return
    }
    //logger.info(`后处理：${msg}`)
    //提取操作
    // 正则表达式，用于匹配方括号之间的内容
    let regex = /\[(.*?)\]/

    // 使用正则表达式提取内容
    let match = msg.match(regex)

    // 检查是否有匹配结果
    if (match) {
        let jsonStr = "[" + match[1].trim() + "]"

        try {
            // 尝试将提取的字符串解析为JSON对象
            logger.debug(`执行操作：${jsonStr}`)
            let jsonObj = JSON.parse(jsonStr)

            let rq = {}
            for(let i = 0; i < jsonObj.length; i++) {
                let son = jsonObj[i]
                rq[son["操作"]] = Events.sharedApi["exOperate"](son["操作"], son["参数"])
            }

            chat(`[执行结果]${JSON.stringify(rq)}`, rFun)
        } catch (e) {
            chat(`[执行失败]执行异常：${e}`, rFun)
        }
    } else {
        // 没有找到匹配的内容
        rFun(msg)
    }
}

Events.sharedApi["AIChat"] = chat

module.exports = {
    "name": "AI请求组件",
    "version": "1.0.0"
}