/*语音*/
const Config = require('../Config.json')
if(Config["交互模式"] !== "语音") return
const { Logger } = require("./.logger")
const logger = new Logger("Voice")
const { Events } = require("./.Events")
const Nls = require("alibabacloud-nls")
const recorder = require('node-record-lpcm16')
const RPCClient = require('@alicloud/pop-core').RPCClient

let client = new RPCClient({
    accessKeyId: Config["语音识别"].accessKeyId,
    accessKeySecret: Config["语音识别"].accessKeySecret,
    endpoint: 'http://nls-meta.cn-shanghai.aliyuncs.com',
    apiVersion: '2019-02-28'
})

Events.sharedApi["isStopVoice"] = false
function start() {
    client.request('CreateToken').then((result) => {
        let TOKEN = result.Token.Id
        logger.warn(`Token获取成功:${TOKEN}`)

        let st = new Nls.SpeechTranscription({
            url: Config["语音识别"].url,
            appkey: Config["语音识别"].appKey,
            token:TOKEN
        })

        st.on("end", (msg)=>{
            if(Events.sharedApi["isStopVoice"]) return
            msg = JSON.parse(msg).payload.result
            logger.debug(msg)
            if(Config["识别模式"] === "sIf") {
                if(msg.includes("罗寓丹")) {
                    Events.sharedApi["AIChat"](msg, (rMsg) => {
                        Events.sharedApi["isStopVoice"] = true
                        Events.sharedApi["synthesis"](rMsg, () => {
                            Events.sharedApi["isStopVoice"] = false
                        })
                    })
                }
            } else if(Config["识别模式"] === "aiIf") {
                Events.sharedApi["exChat"]("[用户]" + msg, (st) => {
                    if(st) {
                        Events.sharedApi["AIChat"](msg, (rMsg) => {
                            Events.sharedApi["exChat"]("[罗寓丹]" + rMsg, () => {})
                            Events.sharedApi["isStopVoice"] = true
                            Events.sharedApi["synthesis"](rMsg, () => {
                                Events.sharedApi["isStopVoice"] = false
                            })
                        })
                    }
                })
            } else logger.error("识别模型配置错误，可用配置项：[sIf:规则判断|aiIf:AI判断]")
        })

        st.start(st.defaultStartParams(), true, 6000)

        let recording = recorder.record({
            sampleRate: 16000, // 采样率 (Hz)
            channels: 1, // 声道数
            audioType: 'wav', // 音频类型
        })
        recording.stream().on("data", (data) => {
            if(Events.sharedApi["isStopVoice"]) return
            //logger.info(`发送数据:${data.length}`)
            st.sendAudio(data)

            if((new Date(result.Token.ExpireTime).getTime() * 1000) < new Date().getTime()) {
                logger.warn(`Token过期，正在刷新...`)
                recording.stop()
                st.close()
                start()
            }
        })
    })
}
start()

module.exports = {
    "name": "语音",
    "version": "1.0.0"
}