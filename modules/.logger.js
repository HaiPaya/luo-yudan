const Config = require('../Config.json')

class Logger {
    /**
     * 创建日志对象
     * @param { String } title 日志头
     */
    constructor (title) {
        this.title = title
        return true
    }

    debug(msg) {
        if(!Config.isDeBug) return false
        let date = new Date()
        console.info(`\x1B[36m${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}\x1B[2m DEBUG\x1B[0m [${this.title}] ${msg}`)
        return true
    }
    
    info(msg) {
        let date = new Date()
        console.info(`\x1B[36m${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}\x1B[32m INFO\x1B[0m [${this.title}] ${msg}`)
        return true
    }
    
    warn(msg) {
        let date = new Date()
        console.info(`\x1B[36m${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}\x1B[33m WAIN\x1B[0m [${this.title}] ${msg}`)
        return true
    }
    
    error(msg) {
        let date = new Date()
        console.info(`\x1B[36m${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}\x1B[31m ERROR\x1B[0m [${this.title}] ${msg}`)
        return true
    }
}

module.exports = {
    "name": "日志系统",
    "version": "0.1.0",
    "Logger": Logger
}