/*操作*/
const { Logger } = require("./.logger")
const logger = new Logger("Operate")
const { Events } = require("./.Events")
const Config = require("../Config.json");

function exOperate(name, par) {
    if(name === "添加日程") {
        return "成功"
    } if(name === "添加倒计时") {
        let time = Number(par.time)
        setTimeout(() => {
            if(Config["交互模式"] === "语音") {
                Events.sharedApi["isStopVoice"] = true
                Events.sharedApi["synthesis"](`倒计时“${par.name}”结束了!`, () => {Events.sharedApi["isStopVoice"] = false})
            } else if(Config["交互模式"] === "文本") {
                logger.warn(`倒计时“${par.name}”结束了!`)
            }
        }, 1000 * time)
        return "成功"
    } else return `没有名为${name}的操作`
}

Events.sharedApi["exOperate"] = exOperate

module.exports = {
    "name": "操作",
    "version": "1.0.0"
}