/*AI对话*/
const Config = require('../Config.json')
if(Config["交互模式"] !== "文本") return
const { Logger } = require("./.logger")
const logger = new Logger("AIChat")
const { Events } = require("./.Events")
const readline = require('readline')
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

function getInput() {
    rl.question(">", (msg) => {
        Events.sharedApi["AIChat"](msg, (rMsg) => {
            logger.info(`罗寓丹：${rMsg}`)
            getInput()
        })
    })
}

Events.listen("sys.start", () => {
    getInput()
})


module.exports = {
    "name": "AI对话",
    "version": "1.0.0"
}