/*提取*/
const { Logger } = require("./.logger")
const logger = new Logger("Extract")
const request = require('request')
const {AIChat} = require("./generate")
const { Events } = require("./.Events")
const { exOperate } = require("./operate")
const Config = require('../Config.json')
const fs = require('fs')


let httpPath = "https://dashscope.aliyuncs.com/api/v1/services/aigc/text-generation/generation"
let model = "qwen-max"
let parameters = {
    "top_p": 0.1, //0-1.0 随机性
    "top_k": 1,//0-100 随机性
    "temperature": 0,//0-2 多样性
    "repetition_penalty": 1.0,//1.0-～ 重复惩罚
    "enable_search": false//搜索
}

function getSysRole() { return`
作为一个中间层，你将会看到用户和名为‘罗寓丹’的AI对话。
请根据语境判断用户是否在和‘罗寓丹’说话，是的话请启动‘罗寓丹’。
你也可以根据对话中隐藏的需求判断是否需要启动‘罗寓丹’。

如果不是，请返回‘false’，不启动‘罗寓丹’。
如果是，请返回‘true’，启动‘罗寓丹’。
`}

let chatList = []
//对话
function chat(msg, fun) {
    try {
        //构建内容
        let ncontent = [{"text": `${msg}`}]

        //判断是否已经创建对话
        if(chatList.length === 0) {
            //没有
            chatList = [
                {
                    "role": "system",
                    "content": [
                        {
                            "text": getSysRole()
                        }
                    ]
                },
                {
                    "role": "user",
                    "content": ncontent
                }
            ]
            request.post(httpPath, {
                "headers": {
                    "Content-Type": "application/json",
                    "Authorization": Config["推理"].apiKey
                },
                "json": {
                    "model": model,
                    "input": {
                        "messages": chatList
                    },
                    "parameters": parameters
                }
            }, (err, res, body) => {
                //logger.info(JSON.stringify(body))
                if (err) {
                    logger.error(`请求出错:${err}`)
                    fun()
                    return
                }
                if(body.output == null) {
                    logger.error(`请求出错:${JSON.stringify(body)}`)
                    fun()
                    return
                }
                chatList.push({
                    "role": "assistant",
                    "content": body.output.text
                })
                echat(body.output.text, fun)
            })
        }
        else {
            //有
            //处理历史记录
            if(chatList.length >= (3 * 2) + 1) {
                chatList.splice(1, 2)
            }

            chatList[0].content[0].text = getSysRole()
            chatList.push({
                "role": "user",
                "content": ncontent
            })

            request.post(httpPath, {
                "headers": {
                    "Content-Type": "application/json",
                    "Authorization": Config["推理"].apiKey
                },
                "json": {
                    "model": model,
                    "input": {
                        "messages": chatList
                    },
                    "parameters": parameters
                }
            }, (err, res, body) => {
                //logger.info(JSON.stringify(body))
                if (err) {
                    logger.error(`请求出错:${err}`)
                    fun()
                    return
                }
                if(body.output == null) {
                    logger.error(`请求出错:${JSON.stringify(body)}`)
                    fun()
                    return
                }
                chatList.push({
                    "role": "assistant",
                    "content": body.output.text
                })
                echat(body.output.text, fun)
            })
        }
    }
    catch(err) {
        logger.error(`请求出错:${err}`)
        fun()
    }
}

//后处理
function echat(msg, rFun) {
    logger.debug(`中间层：${msg}`)

    if (msg.includes("false")) rFun(false)
    else rFun(msg)
}

Events.sharedApi["exChat"] = chat

module.exports = {
    "name": "提取",
    "version": "1.0.0"
}