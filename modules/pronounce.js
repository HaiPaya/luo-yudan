/*发音*/
const Config = require('../Config.json')
if(Config["交互模式"] !== "语音") return
const { Logger } = require("./.logger")
const logger = new Logger("Pronounce")
const { Events } = require("./.Events")
const fs = require('fs')
const Nls = require('alibabacloud-nls')
const { exec } = require('child_process')
const RPCClient = require('@alicloud/pop-core').RPCClient

let client = new RPCClient({
    accessKeyId: Config["语音合成"].accessKeyId,
    accessKeySecret: Config["语音合成"].accessKeySecret,
    endpoint: 'https://nls-meta.cn-shanghai.aliyuncs.com',
    apiVersion: '2019-02-28'
})

function start(msg, fun) {
    client.request('CreateToken').then((result) => {
        let TOKEN = result.Token.Id
        logger.info(`合成音频:${msg}`)

        let tts = new Nls.SpeechSynthesizer({
            url: Config["语音合成"].url,
            appkey: Config["语音合成"].appKey,
            token: TOKEN
        })

        let audioData = []; // 存储接收到的音频数据

        tts.on('data', (dMsg) => {
            audioData.push(dMsg) // 将音频数据存储到数组中
        })

        tts.on('completed', () => {
            const audioBuffer = Buffer.concat(audioData); // 将接收到的音频数据合并为一个Buffer
            fs.writeFile('tmp/output.wav', audioBuffer, (err) => {
                exec("afplay tmp/output.wav", () => {
                    setTimeout(() => fun(), 500)
                })
            })
        })

        let param = tts.defaultStartParams()
        param.text = msg
        param.voice = Config["语音合成"].voice
        param.speech_rate = Config["语音合成"].speech_rate
        tts.start(param, true, 6000)
    })
}

Events.sharedApi["synthesis"] = start

module.exports = {
    "name": "发音",
    "version": "1.0.0"
}