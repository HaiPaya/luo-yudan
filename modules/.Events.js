/*事件系统*/
const { Logger } = require("./.logger")
const logger = new Logger("Events")

var EventsList = {}


/**
 * 监听事件
 * @param {String} EventName 事件名称
 * @param {Function} fun 调用函数
 */
function listen(EventName, fun) {
    logger.debug(`监听事件: ${EventName}`)
    if(EventsList[EventName] == null) EventsList[EventName] = []
    EventsList[EventName].push(fun)
}


/**
 * 触发事件
 * @param {String} EventName 事件名称
 * @param {...any} args 调用函数
 */
function on(EventName, ...args) {
    logger.debug(`触发事件: ${EventName}`)
    if(EventsList[EventName] == null) EventsList[EventName] = []
    EventsList[EventName].forEach((fun) => {
        fun(...args)
    })
}

let sharedApi = {}

module.exports = {
    "name": "事件系统",
    "version": "0.1.0",
    "Events": {
        "sharedApi": sharedApi,
        "listen": listen,
        "on": on
    }
}